<?php



// exercise 1
//Déclarer une variable de type array Associatif qui stocke les informations suivantes 
// puis afficher dans des balise de type liste  avec des class de couleurs les clés et valeurs de tous les éléments du tableau en utilisant la boucle foreach.
    // France => Paris
    // Allemagne => Berlin
    // Italie => Rome


//    $villes = array(
//       'France' => 'Paris',
//       'Allemagne' => 'Berlin',
//       'Italie' => 'Rome'
//    );
//    foreach($villes as $ville => $capital) {
//       echo '<ul class="list-unstyled">
//       <li class="text-center text-success">'.$ville.' : ' .$capital.' </ul>';	   
//    }

























// exercise 2

//En utilisant la fonction rand(), remplir un tableau avec 10 nombres aléatoires. Puis, tester si le chiffre 42 est dans le tableau et afficher un message en conséquence. 
// $randomNumber = array(rand(0,50));
//    if(in_array(42, $randomNumber))
//       echo 'Le nombre 42 est bien dans le tableau.';
//    else
//       echo 'Le tableau ne contient pas la valeur 42.';
//    echo '<br />';






























//exercise 3
//En utilisant le tableau ci-dessous, afficher seulement les pays qui ont une population supérieure ou égale à 20 millions d'habitants.
//    $pays_population = array(
//       'France' => 67595000,
//       'Suede' => 9998000,
//       'Suisse' => 8417000,
//       'Kosovo' => 1820631,
//       'Malte' => 434403,
//       'Mexique' => 122273500,
//       'Allemagne' => 82800000,
//    );

//     echo '<p>Les pays suivants ont une population supérieure à 20 millions d\'habitants.</p><ul>';
//    foreach($pays_population as $pays => $population) {
//       if($population >= 20000000) {
// 	     echo '<li>'.$pays.'</li>';
//       }
//    }
//    echo '</ul>';























   // exercise 4
   //Quelle syntaxe permet  de chercher la valeur  Mojito  du tableau $cocktails et de l'afficher si elle existe ?
//    $cocktails = array('Mojito', 'Long Island Iced Tea', 'Gin Fizz', 'Moscow mule','Mojito');

//    if(in_array("Mojito",$cocktails))
//    {
//        echo "Mojito existe dans le tableau </br>";
//    }else
//    {
//        echo "Mojito ne se trouve pas dans le tableau </br>";
//    }
// // puis afficher le nombre d'élément total du tableau

// echo "il y à " .count($cocktails). " cocktails dans le tableau </br>";
















//exercise 5 
// créer un tableau de jeux vidéo minimum 5 éléments.
//puis ajouter un élément supplémentaire.
// afficher le dernier élément que vous venez d'inserer.

// $games = array("assasin creeds","mario","ark","shenmue","cod");

// array_push($games,"god of war");
//  $first = reset($games);// récupère le premeir élément du tableau
//  $last = end($games);// récupère le dernier élément du tableau
//  echo $last."</br>";
 //echo $first."</br>";























// un dernier pour la route...
//créer un tableau avec 10  prénoms dedans  puis  avec la fonction rand() afficher un prénom au hazard qui se trouve dans le tableau

$pseudos = array("Max","Veroni","Nath","Brandon","Clement","Sebastien","Nael","Antoine","Mac","David");

$random_N = rand(0,10);

echo $pseudos[$random_N];


?>
<!doctype html>
<html lang='fr'>
<head>
<meta charset='utf-8'>
<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<title>Intro à php</title>
<meta name='description' content='your description'>
<meta name='viewport' content='width=device-width,initial-scale=1'>
<link href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' rel='stylesheet'>
</head>
<body>
    <section>
        <a class="btn btn-primary" href="../index.php">back to home</a>
    </section>
</body>
</html>