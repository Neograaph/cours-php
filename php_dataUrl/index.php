<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP DataUrl</title>
</head>
<body>
  <section>
    <ul>
      <li><a href="./views/hello.php?pseudo=Neograph&amp;age=25">Dis moi bonjour</a></li>
      <li><a href="./views/repeat.php?firstname=maxime&amp;lastname=gauthier&amp;repeat=5">Repeat</a></li>
    </ul>
  </section>
</body>
</html>