<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Formulaires PHP</title>
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/css/bootstrap.min.css' />
</head>

<body>
  <section>
    <h1 class='text-center mb-5'>Formulaires PHP</h1>
  </section>
  <section class="d-flex justify-content-center">
    <form class='d-flex gap-1' action="./views/register.php" method='post'>
      <input class='form-control' name='name' type="text" placeholder="Name" required autofocus>
      <input class='form-control' name='lastName' type="text" placeholder="Last Name" required>
      <input class='form-control' name='email' type="text" placeholder="Email" required>
      <input class='form-control' name='age' type="number"  placeholder='Age' required>
      <input class='form-control' name='password' type="password"  placeholder='Password' required>
      <div>
        <input type="radio" id="homme" name="sexe" value="Homme" checked>
        <label for="homme">Homme</label>
        <input type="radio" id="femme" name="sexe" value="Femme">
        <label for="femme">Femme</label>
      </div>
      <select name="pays" id="pays">
        <option value="France">France</option>
        <option value="Allemagne">Allemagne</option>
        <option value="Espagne">Espagne</option>
      </select>
      <input class='btn btn-primary' type="submit" value="Confirmer">
    </form>
  </section>
</body>

</html>

<!-- 
// maintenant à vous de créer un formulaire complet et récuperer les informations suivantes.
// email,password,nom,prenom,genre,age,pays.
// utiliser les input adéquat pour chaque type de donner
// utiliser des select pour le pays avec quelques options de pays  minimum 3. 
// utiliser des checkbox ou radio pour le genre masculin ou feminin on ne peut choisir qu'une seule options.
// utiliser le type number pour l'age et vérifier bien que vous recevrer un nombre int
// puis affichez les données reçu comme l'exemples précédant en sécurisant les paramétres.
// pensez à échapper le html pour éviter les failles xss -->