<!DOCTYPE html>
<html lang="en">
<head>
  <?php
  include('./views/layout/head.php');
  ?>
</head>
<body>
  <?php
  include('./views/layout/nav.php');
  include('./views/layout/header.php');
  include('./views/layout/footer.php');
  ?>
</body>
</html>

<!-- 
construisez une page web php type site vitrine compléte avec les élément suivant: (baser vous sur le modéle du templating)

    un layout head avec bootstrap integrer en css et toutes les balise meta importante.
    inserer dans la balise meta title, un echo avec votre title écrit en variable php.
    idem pour la meta description.
    une barre de navigation avec 4 lien qui affichera les autres pages créer dans un dossier nommer views, ces liens seront nommer ainsi: home,about,services,tarifs
    un header type HERO avec une image ou une vidéo en background representant votre activité principale prenant toutes la largeur.
    une page about avec une présentation de votre activité et un paragraphe sur vous même créer en php avec echo
    une page services avec une liste des services que vous offrez.
    une page tarif avec 3 tarifs que vous proposer en package en rapport à vos services, utiliser des class card de bootstrap ou mbd
    un footer avec copyright créer dans le dossier layout
    la page web doit être responsive sur tout les écrans (smartphone,tablette,laptop et desktop) et designer un minimum.

    ps: mettez un peu de couleur ;)

 -->