<?php
$pseudos = ['Max','Mac','Nhat','Brandon'];
$prenoms = array ('Arthur','Sebastien','Nael','Véronique','Clément');
$pseudo = $pseudos[0];
$prenom = $prenoms[4];

// $coordonnees['prenom']='François';
// $coordonnees['nom']='Dupont';
// $coordonnees['adresse']='3 Rue du Paradis';
// $coordonnees['ville']='Marseille';

$coordonnees = array (
  'prenom' => 'Francois',
  'nom' => 'Dupont',
  'adresse' => '3 Rue du Paradis',
  'ville' => 'Marseille',
);




?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/css/bootstrap.min.css'>
    <title>PHP</title>
  </head>
  <body>
    <section>
      <a class="btn btn-success <?php echo 'container-fluid' ?>" href="../index.php">Retour sur l'index</a>
      <div>
        <h1 class="display-1 text-center">Les Arrays PHP</h1>
      </div>
    </section>
    <section>
      <div>
        <p class="text-center">
          <?php 
          echo "Bonjour ".$pseudo;
          echo "<br/>";
          echo $coordonnees["ville"];
          echo "<br/>";
          echo "<br/>";

          $prenoms = array ('Francois','Michel','Nicole','Véronique','Benoît');
          echo "Boucle for<br/>";
          for ($numero=0;$numero<5;$numero++){
            echo $prenoms[$numero].'<br/>';
          };

          echo "<br/>";
          echo "<br/>";

          echo "Boucle foreach<br/>";
          foreach ($prenoms as $prenom){
            echo $prenom.'<br/>';
          };

          echo "<br/>";
          echo "<br/>";

          foreach ($coordonnees as $coordonnee){
            echo $coordonnee.'<br/>';
          };

          echo "<br/>";
          echo "<br/>";

          echo "Boucle foreach avec la clée<br/>";
          foreach ($coordonnees as $coordonnee => $value){
            echo $coordonnee.': '.$value.'<br/>';
          };

          echo "<br/>";
          echo "<br/>";

          $exemple=15;
          print_r($coordonnees);
          echo "<br/>";
          // var_dump affiche le tye d'une variable ainsi que sa valeur
          var_dump($exemple);
          // print($coordonnees);

          echo "<br/>";
          echo "<br/>";

          if(array_key_exists('nom',$coordonnees)){
            echo 'La clé "nom" se trouve dans les coordonnées !';
          }
          echo "<br/>";
          if(!array_key_exists('pays',$coordonnees)){
            echo 'La clé "pays" ne se trouve pas dans les coordonnées !';
          }

          echo "<br/>";
          echo "<br/>";

          $fruits=array ('banane','pomme','poire','cerise','fraise','framboise');
          if(!in_array('myrtille',$fruits)){
            echo 'La valeur "Myrtille" ne se trouve pas dans les fruits !';
          }
          else{
            echo 'La valeur "Myrtille" se trouve dans les fruits';
          }

          echo "<br/>";
          echo "<br/>";

          $position = array_search('fraise',$fruits);
          $number_fruits=count($fruits);
          echo 'La fraise de trouve à l\'emplacement '.$position.'<br/>';
          echo 'Nombre de fruit dans le tableau: '.$number_fruits;
          ?>
        </p>
      </div>
    </section>
</body>
</html>