<!DOCTYPE html>
<html lang="en">
<head>
  <?php 
  $title = "Exo 1";
  include("../views/layout/head.php") 
  ?>
</head>
<body>
  <?php include("../views/layout/navback.php") ?>

  <!-- exo 1 -->
  <section>
    <?php 
    $capitales = array(
      'France' => 'Paris',
      'Allemagne' => 'Berlin',
      'Italie' => 'Rome',
    )
    ?>
    <div>
      <h2>Exo 1</h2>
      <ul>
      <?php
      foreach ($capitales as $ville => $capitale){
        echo "<li><span class='text-primary'>".$ville."</span>: <span class='text-danger'>".$capitale."</span></li>";
      }
      ?>
      </ul>
    </div>
  </section>

  <!-- exo 2 -->
  <section>
    <?php
      $nombres = array ();
      for ($lenght=0;$lenght<10;$lenght++){
        $nombres[] = rand(0,100);
      };
      // var_dump($nombres);
      // echo $lenght;
    ?>
    <div>
     <h2>Exo 2</h2>

    <?php
      if(in_array(42,$nombres)){
        echo 'il y a un 42';
      }
      else{
        echo 'pas de 42';
      }
    
    ?>
    </div>
  </section>

  <!-- exo 3 -->
  <section>
    <?php
      $pays_population = array(
        'France' => 67595000,
        'Suede' => 9998000,
        'Suisse' => 8417000,
        'Kosovo' => 1820631,
        'Malte' => 434403,
        'Mexique' => 122273500,
        'Allemagne' => 82800000,
    );
    ?>
    <div>
      <h2>Exo 3</h2>
      <div>
        <?php
          foreach ($pays_population as $pays => $nbr){
            if ($nbr>20000000){
              echo $pays.'<br/>';
            }
          }
        ?>
      </div>
    </div>
  </section>

  <!-- exo 4 -->
  <section>
    <?php
      $cocktails = array('Mojito', 'Long Island Iced Tea', 'Gin Fizz', 'Moscow mule','Mojito');
    ?>
    <div>
      <h2>Exo 4</h2>
      <div>
        <?php
          if(in_array('Mojito',$cocktails)){
            echo 'Mojito est présent dans le tableau !';
          }
          echo '<br/>';
          echo count($cocktails).' éléments dans le tableau';
        ?>
      </div>
    </div>
  </section>

  <!-- exo 5 -->
  <section>
    <?php
      $jeuxV=array ('Overwatch','LeagueOfLegends','CounterStrike','Battlefield','CallOfDuty');
    ?>
    <div>
      <h2>Exo 5</h2>
      <div>
        <?php
          // var_dump($jeuxV);
          // echo '</br>';
          $jeuxV[] = 'ViaFight';
          // echo '</br>';
          // var_dump($jeuxV);
          // echo '</br>';
          echo $jeuxV[5].' (dispo sur toutes les plateformes de téléchargement(faux))';
        ?>
      </div>
    </div>
  </section>

  <!-- exo 6 -->
  <section>
    <?php
      $prenoms = array ('Francois','Michel','Nicole','Véronique','Benoît','Francois','Michel','Nicole','Véronique','Benoît');
    ?>
    <div>
      <h2>Exo 6</h2>
      <div>
        <?php
          echo $prenoms[rand(1,10)];
        ?>
      </div>
    </div>
  </section>
</body>
</html>

<!-- exercice 1

déclarer une var de type array asso qui sotck les informations suivantes
puis afficher dans des balises de type liste avec des class de couleurs les clés et valeurs de tous les éléments du tableau en utilisant la boucle foreach
France => Paris
Allemagne => Berlin
Italie => Rome -->

<!-- exercice 2

En utilisant la fonction rand(), remplir un tableau avec 10 nombres aléatoires. Puis, tester si le chiffre 42 est dans le tableau et afficher un message en conséquence. 
$randomNumber = rand(0,50);
echo $randomNumber; -->

<!-- exercise 3
//En utilisant le tableau ci-dessous, afficher seulement les pays qui ont une population supérieure ou égale à 20 millions d'habitants.
$pays_population = array(
    'France' => 67595000,
    'Suede' => 9998000,
    'Suisse' => 8417000,
    'Kosovo' => 1820631,
    'Malte' => 434403,
    'Mexique' => 122273500,
    'Allemagne' => 82800000,
); -->

<!-- exercise 4
  Quelle syntaxe permet  de chercher la valeur  Mojito  du tableau $cocktails et de l'afficher si elle existe ?
  $cocktails = array('Mojito', 'Long Island Iced Tea', 'Gin Fizz', 'Moscow mule','Mojito');
puis afficher le nombre d'élément total du tableau -->

<!-- exercise 5 
créer un tableau de jeux vidéo minimum 5 éléments.
puis ajouter un élément supplémentaire.
afficher le dernier élément que vous venez d'inserer. -->

<!-- un dernier pour la route...
créer un tableau avec 10  prénoms dedans  puis  avec la fonction rand() afficher un prénom au hazard qui se trouve dans le tableau -->