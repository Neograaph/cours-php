<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.0/css/bootstrap.min.css'>
  <title>Document</title>
</head>
<body>
  <section>
    <div>
      <a class="btn btn-success <?php echo 'container-fluid' ?>" href="../index.php">Retour sur l'index</a>
    </div>
    <p>cette ligne est en html</p>
    <?php
    echo "ceci est écrit en php avec echo";
    print "</br>";
    print "ceci est écrit en php avec print";
    ?>
  </section>

</body>
</html>