<!DOCTYPE html>
<html lang="en">
<head>
  <?php 
  $title = "PhP";
  include("./views/layout/head.php"); ?>
  ?>
</head>
<body>

  <section>
    <div>
      <h1 class="d-flex justify-content-center">
        PHP Introduction
      </h1>
      <div class="d-flex justify-content-center gap-1">
        <a class="btn btn-danger" href="./pages/info.php">Info PHP</a>
        <a class="btn btn-success" href="./pages/showtxt.php">Afficher du texte en PHP</a>
        <a class="btn btn-success" href="./pages/date.php">Afficher la date</a>
        <a class="btn btn-success" href="./pages/balise.php">Afficher la balise</a>
      </div>
    </div>
  </section>

  <br><br><br>
  
  <section>
    <?php
    $pseudo="Neograph";
    $age=25;
    $logged=false;
    $weight=65.5;
    $newsletterSub=null;
    ?>
    <div>
      <h2 class="d-flex justify-content-center">
        PHP Suite:
      </h2>
    </div>
    <div>
      <p class="text-center justify-content-center">
        <?php
        echo "Bonjour ".$pseudo." tu as ".$age." ans et tu pèses ".$weight." Kg";
        ?>
        <br>
        <a class="btn btn-success disabled">Changer la valeur de logged</a>
      </p>
      <p class="d-flex justify-content-center">
      <?php
      if ($logged){
        echo $pseudo." tu es connecté";
      }
      else{
        echo "<a class='btn btn-success' href='./pages/newUser.php'>Créer un compte</a>";
      }
      ?>
      </p>
    </div>
    <div class="d-flex justify-content-center gap-1">
      <a class="btn btn-primary" href="./pages/conditions.php">Conditions</a>
      <a class="btn btn-primary" href="./pages/boucles.php">Boucles</a>
      <a class="btn btn-primary" href="./pages/fonctions.php">Fonctions</a>
      <a class="btn btn-primary" href="./pages/tableaux.php">Tableaux</a>
    </div>
  </section>

  <br><br><br>

  <section>
    <div>
      <h2 class="d-flex justify-content-center">
        PHP Exercices:
      </h2>
    </div>
    <div class="d-flex justify-content-center gap-1">
      <a class="btn btn-primary" href="./pages/exo1.php">Part 1</a>
    </div>
  </section>
</body>
</html>